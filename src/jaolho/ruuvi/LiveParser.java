package jaolho.ruuvi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class LiveParser {

  public static final int MAC_LENGTH = 6;
  public static final int MAC_START_INDEX = 7;
  
  int packetLength = 40;
	int byteIndex = 0;
	int pointerIndex = 0;
	boolean printed = false;
	
	List<int[]> packets = new ArrayList<int[]>();
	List<RuuviData> ruuviDataList = new ArrayList<RuuviData>();
	
	//BufferedReader s = new BufferedReader(new FileReader(""));
	//FileInputStream fs = new FileInputStream("data.txt");// = new 
	
	
	public static void test() {
	  try {
	    LiveParser p = new LiveParser();
      BufferedReader br = new BufferedReader(new FileReader("src/jaolho/ruuvi/data.txt"));
      int c = br.read();
      while(c != -1) {
        p.write((char) c);
        c = br.read();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
	}
	
	private void addRuuviData(int[] rawData) {
	  
	}
	
	private String parseMAC(int[] rawData) {
	  int[] macArray = Arrays.copyOfRange(rawData, MAC_START_INDEX, MAC_START_INDEX + MAC_LENGTH);
	  List<Object> hexList = Arrays.asList(Arrays.stream(macArray).mapToObj(i -> (String.format("%02X", i))).toArray());
	  Collections.reverse(hexList);
	  return hexList.stream()
	      .map(o -> "" + o)
        .collect(Collectors.joining(":"));
	}
	
	private double parseTemperature(int[] rawData) {
	  return 0;
	}
	
	private double parseHumidity(int[] rawData) {
    return 0;
  }
	
	private double parsePressure(int[] rawData) {
    return 0;
  }
	
	private double parseVoltage(int[] rawData) {
    return 0;
  }
	
	private RuuviData parse(int[] rawData) {
	  RuuviData result = new RuuviData();
	  result.macAddress = parseMAC(rawData);
    return result;
	}
	
	public void write(char c) {
	  // new packet start char
		if (c == '>') {
			byteIndex = 0;
			printed = false;
			packets.add(new int[packetLength]);
			return;
		}
		// no packets detected yet, ignore incoming chars except the start char
		if (packets.size() == 0) {
		  return;
		}
		// if the packet is full, ignore the rest of the bytes
		if (byteIndex == packetLength) {
		  if (!printed) {
		    //System.out.println("Packet filled: " + Arrays.toString(packets.get(packets.size() - 1)));
		    System.out.println(
		        "Packet filled: " +
		        Arrays.toString(Arrays.stream(packets.get(packets.size() - 1)).mapToObj(i -> (String.format("%02X", i))).toArray())
		    );
		    System.out.println(parseMAC(packets.get(packets.size() - 1)));
		    printed = true;
		  }
		  return;
		}
		// hex char, MSB first
		if ((c >= 48 && c <= 57) || (c >= 65 && c <= 70)) {
			if (pointerIndex == 0) {
			  packets.get(packets.size() - 1)[byteIndex] = Integer.decode("0x" + c) << 4;
			  pointerIndex = 1;
			}
			else {
			  packets.get(packets.size() - 1)[byteIndex] += Integer.decode("0x" + c);
				pointerIndex = 0;
				byteIndex++;
				//System.out.println("Packet filled: " + Arrays.toString(packets.get(packets.size() - 1)));
			}
		}
		// ignore non hex chars
	}

	public static void main(String[] args) {
	  test();
	}

}
