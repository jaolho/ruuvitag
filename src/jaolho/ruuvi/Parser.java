package jaolho.ruuvi;

import java.util.Enumeration;

public class Parser {
	public enum names1 {
		MAC,
		TEMPERATURE,
		HUMIDITY,
		PRESSURE,
		BATTERY
	}

	public static final int MAC_START_INDEX = 14;
	public static final int MAC_LENGTH = 12;
	
	public Object[] data;
			
	public double temperature, humidity, pressure, battery;
	public String mac;
	
	private void reset() {
		data = new Object[names1.values().length];
	}
	
	private int[] asciiHexToBinary(String line) {
		
		return null;
		//String[] sBytes = 
	}
//043E250201030135CBA8813FF31902010415FF9904031E194EBF82FFDD000003EF0C8B00000000C5
	public void parseRawLine(String line) {
		reset();
		line = line.replaceAll("[^A-F0-9]", "");
		if (line.length() != 80)
			return;
		// MAC
		String[] mac = new String[MAC_LENGTH / 2];
		for (int i = MAC_LENGTH; i > 0; i--) {
			mac[i] += line.charAt(MAC_START_INDEX + MAC_LENGTH - 1 + i);
			mac[i] += line.charAt(MAC_START_INDEX + MAC_LENGTH + i);
		}
		
	}
	
	public static void main(String args[]) {
		String line = "> 04 3E 25 02 01 03 01 35 CB A8 81 3F F3 19 02 01 04 15 FF 99\r\n" + 
				"  04 03 1E 19 4E BF 82 FF DD 00 00 03 EF 0C 8B 00 00 00 00 C5";
		line = line.replaceAll("[^A-F0-9]", "");
		System.out.println(line);
		System.out.println(line.substring(14, 24));
		System.out.println(new StringBuffer(line.substring(14, 24)).reverse().toString());
		
		//System.out.println(Integer.toString((int) Integer.decode("0xFFFF"), 2));
	}
}
